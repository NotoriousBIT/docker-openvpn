ARG  ALPINE_VERSION=3.14.3

FROM alpine:${ALPINE_VERSION} AS build-openvpn
ARG  OPENVPN_VERSION=2.5.4

RUN apk add --no-cache --update g++ linux-headers openssl-dev lz4-dev lzo-dev linux-pam-dev make

RUN mkdir /build
WORKDIR /build

RUN wget https://swupdate.openvpn.org/community/releases/openvpn-${OPENVPN_VERSION}.tar.gz -O openvpn.tar.gz
RUN tar -zxf openvpn.tar.gz

WORKDIR /build/openvpn-${OPENVPN_VERSION}

RUN ./configure --disable-dependency-tracking
RUN make && make install

FROM alpine:${ALPINE_VERSION}

ENV EASYRSA_REQ_CN='example.com'
ENV EASYRSA_REQ_COUNTRY='DE'
ENV EASYRSA_REQ_PROVINCE='HH'
ENV EASYRSA_REQ_CITY='HH'
ENV EASYRSA_REQ_ORG='example.com'
ENV EASYRSA_REQ_EMAIL='admin@example.com'
ENV EASYRSA_REQ_OU='example.com'

ENV OPENVPN_PROTO='udp'
ENV OPENVPN_TOPOLOGY='subnet'
ENV OPENVPN_SERVER='10.8.0.0 255.255.255.0'
ENV OPENVPN_IFCONFIG_POOL_PERSIST='ipp.txt'
ENV OPENVPN_REDIRECT_GATEWAY='def1'
ENV OPENVPN_DHCP_OPTION_DNS1='208.67.222.222'
ENV OPENVPN_DHCP_OPTION_DNS2='208.67.220.220'
ENV OPENVPN_KEEPALIVE='10 120'
ENV OPENVPN_TLS_AUTH='ta.key'
ENV OPENVPN_CIPHER='AES-256-GCM'
ENV OPENVPN_PERSIST_KEY='true'
ENV OPENVPN_PERSIST_TUN='true'
ENV OPENVPN_STATUS='openvpn-status.log'
ENV OPENVPN_VERB='3'
ENV OPENVPN_EXPLICIT_EXIT_NOTIFY='1'
ENV OPENVPN_REMOTE='my-server-1 1194'
ENV OPENVPN_RESOLVE_RETRY='infinite'
ENV OPENVPN_NOBIND='true'
ENV OPENVPN_ROUTE='fasle'
ENV OPENVPN_DUPLICATE_CN='false'

RUN apk add --no-cache --update bash lz4-libs lzo easy-rsa iptables && rm -rf /var/cache/apk/*

COPY --from=build-openvpn /usr/local/sbin/openvpn /usr/local/sbin/openvpn

RUN ln -s /usr/share/easy-rsa/easyrsa /usr/local/bin/easyrsa
RUN mkdir /etc/easy-rsa

COPY docker-entrypoint.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/docker-entrypoint.sh

COPY gen-client-access.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/gen-client-access.sh

ENTRYPOINT ["docker-entrypoint.sh"]