# docker-opnevpn

An easy to use dockerized OPENVPN server including generation of client certificates.

## Usage
Run the container using this command:
```
$ docker run -it -p 1194:1194/udp --cap-add=NET_ADMIN notoriousbit/openvpn
```

## Persistence
There are two folders you need to mount to your host system in order to have persistence:
- `/etc/easy-rsa`
- `/etc/openvpn`

```
$ docker run -d -v $(pwd)/easy-rsa:/etc/easy-rsa -v $(pwd)/openvpn:/etc/openvpn -p 1194:1194/udp --cap-add=NET_ADMIN notoriousbit/openvpn
```

## Using docker-compose

Have a look at the examples.

## Generate certificates
All certificates are stored in `/etc/openvpn/clients` in the `.ovpn-format`.

On startup a certificate named `autogen-client.ovpn` will be generated automatically.

You can manualy create additional certificates by running `gen-client-access.sh`, which takes the name as an argument.
```
$ docker exec <CONTAINER> gen-client-access.sh Client1
```

### Copy a certificate
There are two options to get certificates from the container: <br>
**First:**
```
$ docker cp <CONTAINER>:/etc/openvpn/clients/autogen-client.ovpn $(pwd)
```
**Second:** Copy the file from the mounted folder.

## Environment
### EASY-RSA
- `EASYRSA_REQ_CN`
- `EASYRSA_REQ_COUNTRY`
- `EASYRSA_REQ_PROVINCE`
- `EASYRSA_REQ_CITY`
- `EASYRSA_REQ_ORG`
- `EASYRSA_REQ_EMAIL`
- `EASYRSA_REQ_OU`

### OPENVPN
- `OPENVPN_PROTO` default: `udp`
- `OPENVPN_TOPOLOGY` default: `subnet` 
- `OPENVPN_SERVER` default: `10.8.0.0 255.255.255.0`
- `OPENVPN_IFCONFIG_POOL_PERSIST` default: `ipp.txt`
- `OPENVPN_REDIRECT_GATEWAY` default: `def1` *(disable by setting `false`)*
- `OPENVPN_DHCP_OPTION_DNS1` default: `208.67.222.222` *(disable by setting `false`)*
- `OPENVPN_DHCP_OPTION_DNS2` default: `208.67.220.220` *(disable by setting `false`)*
- `OPENVPN_KEEPALIVE` default: `10 120`
- `OPENVPN_TLS_AUTH` default: `ta.key`
- `OPENVPN_CIPHER` default: `AES-256-GCM`
- `OPENVPN_PERSIST_KEY` default: `true` *(disable by setting `false`)*
- `OPENVPN_PERSIST_TUN` default: `true` *(disable by setting `false`)*
- `OPENVPN_STATUS` default: `openvpn-status.log`
- `OPENVPN_VERB` default: `3`
- `OPENVPN_EXPLICIT_EXIT_NOTIFY` default: `1` *(only used on `proto udp`)*
- `OPENVPN_REMOTE` default: `my-server-1 1194`
- `OPENVPN_RESOLVE_RETRY` default: `infinite`
- `OPENVPN_NOBIND` default: `true` *(disable by setting `false`)*
- `OPENVPN_ROUTE` default: `false`
- `OPENVPN_DUPLICATE_CN` default: `false`