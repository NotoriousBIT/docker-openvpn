#!/bin/bash
EASYRSA_DIR=/etc/easy-rsa
OPENVPN_DIR=/etc/openvpn
USERNAME=$1

echo "+ Generating certificate for ${USERNAME}"

if [ -z "${USERNAME}" ]
then
  echo "ERROR: No username found!"
  exit 1
fi

if [ -f ${OPENVPN_DIR}/clients/${USERNAME}.ovpn ]
then
  echo "INFO: Client already exists, recreating.."
  RECREAT=true
fi

export EASYRSA_VARS_FILE=${EASYRSA_DIR}/vars
export EASYRSA_REQ_CN=${USERNAME}

if [ -z $RECREAT ]
then
  easyrsa gen-req ${USERNAME} nopass
  easyrsa sign-req client ${USERNAME}
fi

openssl verify -CAfile ${EASYRSA_DIR}/pki/ca.crt ${EASYRSA_DIR}/pki/issued/${USERNAME}.crt

cp ${OPENVPN_DIR}/client.conf ${OPENVPN_DIR}/clients/${USERNAME}.ovpn

# Todo
# ;user nobody
# ;group nobody

echo "<ca>" >> ${OPENVPN_DIR}/clients/${USERNAME}.ovpn
cat ${OPENVPN_DIR}/ca.crt >> ${OPENVPN_DIR}/clients/${USERNAME}.ovpn
echo "</ca>" >> ${OPENVPN_DIR}/clients/${USERNAME}.ovpn

echo "<cert>" >> ${OPENVPN_DIR}/clients/${USERNAME}.ovpn
cat ${EASYRSA_DIR}/pki/issued/${USERNAME}.crt >> ${OPENVPN_DIR}/clients/${USERNAME}.ovpn
echo "</cert>" >> ${OPENVPN_DIR}/clients/${USERNAME}.ovpn

echo "<key>" >> ${OPENVPN_DIR}/clients/${USERNAME}.ovpn
cat ${EASYRSA_DIR}/pki/private/${USERNAME}.key >> ${OPENVPN_DIR}/clients/${USERNAME}.ovpn
echo "</key>" >> ${OPENVPN_DIR}/clients/${USERNAME}.ovpn

echo "<tls-auth>" >> ${OPENVPN_DIR}/clients/${USERNAME}.ovpn
cat ${OPENVPN_DIR}/ta.key >> ${OPENVPN_DIR}/clients/${USERNAME}.ovpn
echo "</tls-auth>" >> ${OPENVPN_DIR}/clients/${USERNAME}.ovpn